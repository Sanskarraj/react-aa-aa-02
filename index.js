const ffmpeg = require('fluent-ffmpeg');

// Input video file path
const inputVideoFilePath = 'wert.mkv';
// Output audio file path
const outputAudioFile = 'output_audio.mp3';

// Construct the ffmpeg command
ffmpeg()
  .input(inputVideoFilePath)
  .audioCodec('libmp3lame') // Specify the audio codec for output
  .output(outputAudioFile)
  .on('end', () => {
    console.log('Audio extraction finished');
  })
  .on('error', (err) => {
    console.error('Error during audio extraction:', err);
  })
  .run();
